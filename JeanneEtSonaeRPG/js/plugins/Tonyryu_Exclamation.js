//=============================================================================
// Tonyryu_Exclamation.js
//=============================================================================

/*:
 * @plugindesc Plugin permettant d'afficher un point d'exclamation proche d'un event (1.00)
 * @author Tonyryu
 *
 *
 * @help Ajouter un mot clè dans la note d'un event pour afficher un point
 * d'exclamation au dessus du personnage :
 *  - \EXCLASUR : Le personnage doit être sur l'event
 *  - \EXCLADEVANT : Le personnage doit être devant l'event
 * 
 */

/*
 * Suivi de version 
 * 1.00 : 22/05/2020 : Tonyryu : Création plugin
 * 
 */

(function() {

    let _Tonyryu_Game_Map_updateEvents = Game_Map.prototype.updateEvents;
    Game_Map.prototype.updateEvents = function() {
        _Tonyryu_Game_Map_updateEvents.call(this);
        
        // Récupérer coordonnées devant personnage
        let x = $gameMap.roundX($gamePlayer._x);
        let y = $gameMap.roundY($gamePlayer._y);
        
        let tabMapEvent = $gameMap.eventsXy(x, y);
        for(mapEvent of tabMapEvent){
            let event = mapEvent.event();
            if(event.note.includes('\EXCLASUR')){
                if(!$gamePlayer.isBalloonPlaying()){
                    $gamePlayer.requestBalloon(1);
                }
            }
        };

        let d = $gamePlayer.direction();
        x = $gameMap.roundXWithDirection($gamePlayer._x, d);
        y = $gameMap.roundYWithDirection($gamePlayer._y, d);
        
        tabMapEvent = $gameMap.eventsXy(x, y);
        for(mapEvent of tabMapEvent){
            let event = mapEvent.event();
            if(event.note.includes('\EXCLADEVANT')){
                if(!$gamePlayer.isBalloonPlaying()){
                    $gamePlayer.requestBalloon(1);
                }
            }
        };
    };
})();